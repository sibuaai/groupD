﻿//-----------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="ELTE">
//     Copyright (c) ELTE All rights reserved.
// </copyright>
// <author>pls</author>
// <date>3/26/2015 14:42</date>
//-----------------------------------------------------------------------
namespace NameChangerBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// constructor of the application
    /// </summary>
    public partial class App : Application
    {
    }
}
