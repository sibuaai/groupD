﻿//-----------------------------------------------------------------------
// <copyright file="NameChangGUI.xaml.cs" company="ELTE">
//     Copyright (c) ELTE All rights reserved.
// </copyright>
// <author>Attila</author>
// <date>3/23/2015 10:22</date>
//-----------------------------------------------------------------------
namespace NameChanger
{
    using System.Windows;
    using System.Windows.Forms;

    /// <summary>
    /// GUI of NameChanger
    /// </summary>
    public partial class NameChangGUI
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NameChangGUI"/> class.
        /// </summary>
        public NameChangGUI()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the Button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void Button1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string pattern = _in.Text;
            string replaceLiteral = _out.Text;

            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();

            DirectoryGetter temp = new DirectoryGetter();
            temp.ApplyingRename(dialog.SelectedPath, pattern, replaceLiteral);
        }
    }
}