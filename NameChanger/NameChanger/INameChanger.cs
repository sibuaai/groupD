﻿//-----------------------------------------------------------------------
// <copyright file="INameChanger.cs" company="ELTE">
//     Copyright (c) ELTE All rights reserved.
// </copyright>
// <author>Norb</author>
// <date>3/23/2015 8:54</date>
//-----------------------------------------------------------------------
namespace NameChanger
{
    /// <summary>
    /// This interface is used for changing names.
    /// </summary>
    public interface INameChanger
    {
        /// <summary>
        /// Renames the specified pattern.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <param name="path">The path.</param>
        /// <param name="replaceLiteral">The replace literal.</param>
        void Rename(string pattern, string path, string replaceLiteral);
    }
}
