﻿//-----------------------------------------------------------------------
// <copyright file="DirectoryGetter.cs" company="ELTE">
//     Copyright (c) ELTE All rights reserved.
// </copyright>
// <author>Norb</author>
// <date>3/23/2015 9:49</date>
//-----------------------------------------------------------------------
namespace NameChanger
{
    using System.IO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DirectoryGetter
    {
        /// <summary>
        /// Applies the renaming.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="replaceLiteral">The replace literal.</param>
        public void ApplyingRename(string source, string pattern, string replaceLiteral)
        {
            string[] folderPaths = Directory.GetDirectories(source, string.Empty, SearchOption.AllDirectories);
            string[] filePaths = Directory.GetFiles(source, "*.*", SearchOption.AllDirectories);
            for (int i = 0; i < filePaths.Length; ++i)
            {
                filePaths[i] = filePaths[i].Remove(0, source.Length);
            }

            for (int i = 0; i < folderPaths.Length; ++i)
            {
                folderPaths[i] = folderPaths[i].Remove(0, source.Length);
            }

            INameChanger fileRenamer = new FileRenamer();
            foreach (string temp in filePaths)
            {
                fileRenamer.Rename(pattern, temp, replaceLiteral);
            }

            INameChanger folderRenamer = new FolderRenamer();
            foreach (string temp in folderPaths)
            {
                folderRenamer.Rename(pattern, temp, replaceLiteral);
            }
        }
    }
}
