﻿//-----------------------------------------------------------------------
// <copyright file="FolderRenamer.cs" company="ELTE">
//     Copyright (c) ELTE All rights reserved.
// </copyright>
// <author>Norb</author>
// <date>3/23/2015 9:21</date>
//-----------------------------------------------------------------------
namespace NameChanger
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FolderRenamer : INameChanger
    {
        /// <summary>
        /// Renames the specified pattern.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <param name="path">The path.</param>
        /// <param name="replaceLiteral">The replace literal.</param>
        public void Rename(string pattern, string path, string replaceLiteral)
        {
            if (path.Contains(pattern))
            {
                string renamedFolder;
                renamedFolder = path.Replace(pattern, replaceLiteral);
                System.IO.Directory.Move(path, renamedFolder);
            }
        }
    }
}
